---
title: 'Open practices'
subtitle: 'Transmedia explorations'
date: 2020-11-21 00:00:00
description: 'Transmedia explorations'
featured_image: '/images/main/'
permalink: 
---
Openness is the foundational layer in my practice and it can be found in three levels: a) I seek for inclusive, collaborative and transparent ways to address social issues, b) I use many Free/Libre and open source software (FLOSS) in my work, and c) I release the outputs of my work under open licenses.

The philosophical basis of my open practices is inline with the idea that every human being is entitled to free, open and equitable access to knowledge and they must participate in the development of the knowledge commons so that the wisdom of all the people are captured.

In reality, the knowledge commons as we know it is extremely skewed both from an access and participation standpoint. The work that I lead in both my professional and personal space are directed towards addressing these gaps using open practices.

# <a id="filmmaking">✂︎ Filmmaking</a>

I am passionate about documentary filmmaking, and have produced, directed (and are currently working on) over a dozen documentary films. In 2017 I became a National Geographic Explorer and produced three films --  “*Gyani Maiya*”, “*Remosam*” and “*Mage Porob*” -- each one capturing the lives of three indigenous communities whose languages are endangered. Some of my films include digital activism for languages in their core. I have used documentary research as a methodology in my 2021 documentary ["*MarginalizedAadhaar*"](https://www.youtube.com/watch?v=1QKt05JW7MU) that is a long-form narrative of exclusions marginalized communities in India face because of Aadhaar, the national digital identification system. These films are mostly released under Creative Commons <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">Attribution</a> (CC-BY) and <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank">Attribution-ShareAlike</a> (CC-BY-SA) Licenses.

### *Gyani Maiya* (2019)
In this 2019 documentary, late Gyani Maiya Sen-Kusunda, and elder of the *Gi Mihaq* (Kusunda) community from western Nepal, narrates how her people who were nomadic once lost their language while settling in villages. The Gi Mihaq (Kusunda) language is now spoken by only one native speaker.
<iframe src="https://player.vimeo.com/video/439168756" width="640" height="293" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
_Trailer of the documenary "Gyani Maiya"_ <a href="https://theofdn.org/film/gyani-maiya/" target="_blank" class="button button--small">MORE ☞</a>

☁︎ Media coverage in the <a href="https://www.edexlive.com/happening/2018/aug/25/o-foundation-and-national-geographic-set-out-to-document-endangered-languages-before-they-are-lost-f-3728.html/" target="_blank">*New Indian Express*</a> <a href="https://web.archive.org/web/20181031102432/http://www.edexlive.com/happening/2018/aug/25/o-foundation-and-national-geographic-set-out-to-document-endangered-languages-before-they-are-lost-f-3728.html" target="_blank">[Archive]</a> and the <a href="https://www.himalkhabar.com/news/116158/" target="_blank">*Himal Khabar*</a> <a href="https://web.archive.org/web/*/https://www.himalkhabar.com/news/116158/" target="_blank">[Archive]</a>.

### *MarginalizedAadhaar* (2020)
As one of the Digital Identity Fellow at Yoti, I investigated the digital identity landscape of India, particularly the national digital ID Aadhaar, through the lenses of the exclusion of marginalized communities. Through a year-long research, I interviewed about ten different communities that are marginalized on the basis of race, languages, gender, access to information on rights and access to public amenities. I also documented perspectives and reactions of many subject experts on various exclusions. I was involved as the writer, producer and director of the documentary whereas many friends and colleagues were involved in different roles who greatly helped shape it.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/sKPuvZAxUPI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

_MarginalizedAadhaar: Assam - a short video of the MarginalizedAadhaar series_

☀︎ [short interview](https://www.youtube.com/watch?v=HKi2UOC52qE) and [publications from this research](https://www.yoti.com/author/subhashishyotifellows-com/)
<a href="https://www.youtube.com/playlist?list=PLIglc3FBUWUy2ZpVr1dGclpLZcYlEr917" target="_blank" class="button button--small">MORE ☞</a>

Some of my other documentaries include “*Who Owns The Content*” (2019), a short film that explores the dilemma in identifying the ownership of cultural knowledge, and "*Karinding*" (2020). *Who Owns The Content* was premiered at 13th Native Spirit Indigenous Film Festival – North Americas, London in 2019 and was nominated for the Official Selection of the First-Time Filmmaker Sessions November 2020.

## ⚗︎ Open Educational Resources (OER)
Most of the educational content (text and multimedia) that I create as a part of my work are available under open licenses -- mostly under the Creative Commons License -- that allow anyone to reuse, make derivatives and even redistribute with modification. In 2015 I founded <a href="http://theofdn.org/openspeaks" target="_blank">OpenSpeaks</a>, a toolkit for language documentation using open practices. I have created and co-created a range of Wikipedia guides (multimedia and text) for over a decade in more than nine languages. I produced two video OER series -- one for the <a href="https://www.youtube.com/playlist?list=PLe81zhzU9tTTuGZg41mXLXve6AMboaxzD" target="_blank">Hindi Wikipedia</a> and another one for <a href="https://www.youtube.com/playlist?list=PLe81zhzU9tTSbi050RLGhRMXSoh-Meu77" target="_blank">Kannada Wikipedia</a>. I was a Juror and a speaker for the OER 16 Conference in Edinburgh, UK, and a speaker in the OER 17 Conference.

### OpenSpeaks
OpenSpeaks was originally incubated in 2015 inside the Wikimedia Commons (Wikipedia’s sister project and an open multimedia repository). I expanded it through 2017 to accommodate a wide range of OERs, open source software among others. I produced three <a href="#filmmaking">documentaries</a> with support from National Geographic Society under the ambit of OpenSpeaks during 2017-2019. Like all open projects, it is now maintained by a volunteer-led community at Wikiversity, another Wikipedia sister project and an open learning platform. In October 2020, I received a small grant from Creative Commons to further OpenSpeaks' curriculum on open content, Creative Commons Licenses, and content framework. OpenSpeaks also won me a widespread support -- including the <a href="https://journalists.org/2017/09/06/meet-onas-2017-mj-bear-fellows-30-digital-journalism-stand-outs/" target="_blank">ONA 2017 MJ Bear Fellowship</a> and a part in <a href="https://mozilla.github.io/leadership-training/round-4/projects/#openspeaks" target="_blank">Mozilla Open Leadership Series</a>. I spoke in two <a href="https://www.ted.com/talks/subhashish_panigrahi_digital_collaboration_to_save_the_languges" target="_blank">TEDx events</a>, <a href="https://theofdn.org/blogs/conference/creative-commons-summit-2019-openspeaks/" target="_blank">Creative Commons Global Summit 2019</a> and 2020, <a href="https://theofdn.org/blogs/conference/wikimania2019/" target="_blank">Wikimania 2019</a> and <a href="https://wikimedia.org.uk/wiki/Celtic_Knot_Conference_2018/Submissions/How_to_create_a_digital_archive_for_indigenous_languages" target="_blank">Celtic Knot Conference 2018</a> among others.

<a href="https://theofdn.org/openspeaks" target="_blank" alt="Open OpenSpeaks homepage" class="button button--small">MORE ☞</a>

## Free/Libre and Open Source software (FLOSS)

### <a href="https://www.mediawiki.org/wiki/Project_Ol_chiki" target="_blank">Project Ol Chiki</a> [![DOI: 10.5281/zenodo.4726566](https://zenodo.org/badge/53941456.svg)](https://zenodo.org/badge/latestdoi/53941456)

I led the development of Project Ol Chiki at the Centre for Internet and Society's Access to Knowledge program which resulted in creation of a font family "Guru Gomke" in the Ol chiki writing system (used to write the Santali language), input methods to type in Santali and supporting educational resources -- all openly-licensed. Indian type designer Pooja Saxena designed the typeface and Wikimedians Jnanaranjan Sahu and Nasim Ali created the tools for the input methods. Many noted Santali-language speakers contributed in the entire development. I led the overall project development and contributed in designing the input method and OERs. As an open source project, Guru Gomke received many other contributions over the time.


<a href="publications" class="button button--small">✍︎ publications</a> <a href="blog" class="button button--small">✐ blogs</a>
