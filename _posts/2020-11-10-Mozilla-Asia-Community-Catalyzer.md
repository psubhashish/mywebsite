---
title: 'Becoming part of Mozilla’s open web movement in South Asia'
date: 2016-11-10 00:00:00
description: Old blog introducing my new role at Mozilla as the Asia Community Catalyzer
featured_image: '/images/page-post/Mozilla-old-logo-handdrawn.jpg'
redirect_from: /post/152995890100/mozilla
permalink: blog/mozilla-asia-community-catalyzer
---
As a volunteer contributor to the openness and the free and open source software movement, and to the[ Mozilla](https://web.archive.org/web/20200822104701/https://mozillians.org/en-US/u/psubhashish/) projects, I have always felt excited about the Mozilla community. Just like the region, the South Asian Mozilla community is pretty diverse and unique. And Mozilla has been rethinking and reshaping its strategies in the recent past to match the community expectations by the new addition of projects and activities.

It was always my dream to be a part of the larger change that Mozilla as a movement is making by using the open web as a tool and an opportunity knocked on my door a few months back when I first learned about the role of[ Asia Community Catalyzer](https://discourse.mozilla.org/t/asia-community-catalyzer-we-re-hiring/8486). I was about to finish my short stint at the[ Wikimedia Foundation](https://meta.wikimedia.org/wiki/User:SPanigrahi_(WMF)), and was already active for over five years in the South Asian-language Wikimedia community in both my[ volunteer](https://or.wikipedia.org/wiki/%E0%AC%AC%E0%AD%8D%E0%AD%9F%E0%AC%AC%E0%AC%B9%E0%AC%BE%E0%AC%B0%E0%AC%95%E0%AC%BE%E0%AC%B0%E0%AD%80:Psubhashish) and[ professional](https://cis-india.org/@@search?SearchableText=subhashish) capacity. I asked myself, “won’t it be exciting to work with another few fast-growing and innovative volunteer communities from around Asia, when I can still contribute to Wikipedia and Wikimedia projects as a volunteer?”. Things went well.

Thanks to Mozilla for finding me suitable for this role, and to the[ Centre for Internet and Society](https://cis-india.org/), which is more of my alma mater than former employer to gladly allowing me to move on with my new role.

So here I am. To begin with, I will work closely with the community leaders from South Asia, specially in India, Bangladesh, Pakistan, Nepal and Myanmar to:

*   Help in conflict resolution to culturally diverse communities
*   Support focus communities and collaboratively design long term strategy and work plans with balanced governance and distributed leadership structures
*   Monitor community activity, health and evaluate impact of the activities, and share learning from the experience, with a special focus on[ diversity and inclusion](https://wiki.mozilla.org/Diversity_and_Inclusion_Strategy), help identify, recruit and train emerging leaders in the communities by engaging with them by creating new campaigns or using existing ones, and with having sync with the global strategy
*   Coordinate with internal teams on a need basis and build new external partnerships with like-minded individuals and organizations with a focus on raising Mozilla’s profile in South Asian nonprofit and technology sphere
*   Document the lessons learned and help the Participation team to globalize/systematize its global strategy and priorities, and better the dialog with the communities in South Asia

The last few weeks have been overwhelming as I am learning a lot about the complex dynamics of the community during several of 1:1 interactions both with some of the South Asian community leaders, and and staff members. These interactions have helped orient my work for this quarter on something that is really important, and that is towards the restructuring of the Indian community.

[Conflicts](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.596.3463&rep=rep1&type=pdf) are always a part of every single open source community and Mozilla is no exception to that. In fact, a country like India that is extremely heterogeneous[ ethnically, linguistically and culturally](http://nsdl.niscair.res.in/jspui/bitstream/123456789/339/1/pdf%204.4%20NISCAIR-Racial-Ethnic-Relgious-Linguistic-Groups-India-Text-Revised.pdf) always has added challenges when an open community like Mozilla has to function. That said, the need for some speedy intervention to the internal conflicts felt this year, and the first[ announcement](https://discourse.mozilla.org/t/asia-community-catalyzer-we-re-hiring/8486) went calling for ideas to deal with it.

A provisional Meta team consisting of both Regional Coaches and other community leaders of different functional teams and task forces, and staff members evaluated the ideas and[ organized](https://wiki.mozilla.org/India/Mozilla_India_Meetup_2016) a planning meetup in the Indian city of Pune in August this year. And the idea was to bring a clear structure with more focus on roles of the community members. The proposed structure will have four teams:

1. A Functional team that will work on specific products or projects e.g. Campus Clubs, Advocacy,
2. A Geographical team to operate from a city or a certain geographical area,
3. A Meta team to function as a binding force and to support and coordinate activities for both the Functional and the Geographical team, and
4. A Focus team that will be temporarily working on any project or activity and is not under the scope of the Functional or Geographical team

The restructuring process will hopefully help better the relationship between the community members themselves and their relation with the Mozilla staff apart from bringing more clarity on roles and accountability of the community.

I am really looking forward to be part of this amazing community in a much wider way and learn from the leaders in this movement I personally care so much. And the restructuring of the Indian community that the provisional Meta team is working on is something I will be playing an active part in in the coming months, and it will also be an opportunity to meet many in the community. I would love to hear from you all on any channel you are comfortable in, I am @subhapa on both Telegram and[ Twitter](https://twitter.com/subhapa), and you can reach out to me over email at subhashish (at) mozilla (dot) com.

*N.B.* _This was originally published on November 10, 2016 (see [archive](https://web.archive.org/web/20161113121019/https://psubhashish.com/post/152995890100/mozilla)) when this site was hosted elsewhere, and was migrated here on October 31, 2020. It it kept here for historical purposes. Content was not edited during migration for factual or grammatical errors._ ☞ [announcement on Mozilla discourse](https://discourse.mozilla.org/t/introducing-our-new-south-asia-community-catalyzer/11975)...
