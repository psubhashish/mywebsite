---
title: 'Media coverage'
subtitle: 'News coverage and citations'
date: 2020-11-19 00:00:00
description: News media coverage and citations
featured_image: '/images/page-post/publications.jpg'
permalink: /media-coverage
---

## 2024
* Anmol Irfan, ["The People vs Anglocentrism"](https://blog.peoplevsbig.tech/the-people-vs-anglocentrism/), People vs Big Tech.
* ["Docufiction Film Bringing Down a Mountain Edited with DaVinci Resolve Studio"](https://www.blackmagicdesign.com/media/release/20240315-01)

## 2023
* Jake Orlowitz. ["Subhashish Panigrahi and meaningful access to the internet in South Asia and beyond"](https://podcast.whoseknowledge.org/posts/ep13/), Whose Voices.

## 2021

* Oreste Pollicino and Giovanni De Gregorio (eds), “Blockchain and Public Law”, Cheltenham: Edward Elgar, 2021.

## 2020
*   “_[UNESCO organises workshop on strengthening multilingualism through datasets in low resourced languages at IGF 2020](https://en.unesco.org/news/unesco-organises-workshop-strengthening-multilingualism-through-datasets-low-resourced)_”. UNESCO. (November 19, 2020)
*   Garcia, Raphael. “_[Preserving Lost Languages](https://www.magzter.com/article/Lifestyle/Readers-Digest-UK/PRESERVING-LOST-LANGUAGES)_”. Reader’s Digest. (October 2020)
*   [Burt, Chris](https://www.biometricupdate.com/author/chris-burt). “Two dozen new Indian insurers approved for eKYC checks with Aadhaar biometrics”. Biometricupdate.com. (August 20, 2020)
*   [Pascu, Luana](https://www.biometricupdate.com/author/luanapascu). “_[Yoti announces new partnership, digital identity podcast, approval for online child protection in Germany: Numbered Humans podcast introduced](https://www.biometricupdate.com/202005/yoti-announces-new-partnership-digital-identity-podcast-approval-for-online-child-protection-in-germany)_”. Biometricupdate.com. (May 29, 2020)
*   Good ID team. "_[Marginalized Aadhaar: Digital Identity in the Time of COVID-19– Yoti](https://www.good-id.org/en/articles/marginalized-aadhaar-digital-identity-time-covid-19/)_". (May 26, 2020)
*   [Burt, Chris](https://www.biometricupdate.com/author/chris-burt). “_[Biometrics offerings for keeping people safe at work and making payments jostle for market position](https://www.biometricupdate.com/202005/biometrics-offerings-for-keeping-people-safe-at-work-and-making-payments-jostle-for-market-position)_”. Biometricupdate.com. (May 16, 2020)
*   “_[Yoti Fellow Suggests COVID-19 Has Exacerbated Issues with Aadhaar System](https://findbiometrics.com/yoti-fellowship-suggests-covid-19-exacerbated-issues-aadhaar-system-051208/)_”. Findbiometrics.com (May 12, 2020)
*   “_[Facial recognition datasets and controversies drive biometrics and digital ID news of the week](https://www.biometricupdate.com/202001/facial-recognition-datasets-and-controversies-drive-biometrics-and-digital-id-news-of-the-week)_”. Biometricupdate.com. (January 31, 2020)
* “India: Freedom on the Net 2020 Country Report.” Freedom House, https://freedomhouse.org/country/india/freedom-net/2020. Accessed 17 Mar. 2022.

## 2019
*   “[Yoti Fellowship Examines National ID Program in South Africa](https://findbiometrics.com/yoti-fellowship-examines-national-id-program-south-africa-112905/)”. Findbiometrics.com (November 29, 2019)
*   Burt, Chris. “[Biometrics benefits push Aadhaar to 92 percent satisfaction rate but exclusion problems remain](https://www.biometricupdate.com/201911/biometrics-benefits-push-aadhaar-to-92-percent-satisfaction-rate-but-exclusion-problems-remain)". Biometricupdate.com. (November 26, 2019)
*   “_[Yoti Fellowship winners](https://www.tribuneindia.com/news/archive/jobs-careers/yoti-fellowship-winners-830958)_". The Tribune. (Sep 11, 2019)
*   “_[Yoti Selects its Digital Identity Research Fellows](https://mobileidworld.com/yoti-selects-its-digital-identity-research-fellows-809041/ )_”. MobileIDWorld  (September 4, 2019)
*    “_[Indian boy working for marginalised communities wins Yoti fellowship](https://www.educationtimes.com/article/editors-pick/71012586/indian-boy-working-for-marginalised-communities-wins-yoti-fellowship)_". Education Times. (September 06, 2019)
*    “_[Say hello to the new 2019 Yoti Digital Identity Fellows– Yoti](https://www.good-id.org/en/articles/say-hello-new-2019-yoti-digital-identity-fellows/ )_" Good ID. (September 3, 2019)

## 2018
* Abrougui, Afef. “South Asian Governments Keep Ordering Internet Shutdowns — and Leaving Users in the Dark.” Ranking Digital Rights, 11 Sept. 2018, https://rankingdigitalrights.org/2018/09/11/south-asian-governments-keep-ordering-internet-shutdowns-and-leaving-users-in-the-dark/.

## 2012
* "[Wikipedia’s cultural mission in India](https://www.csmonitor.com/World/Global-News/2012/0202/Wikipedia-s-cultural-mission-in-India)". Christian Science Monitor. (February 2, 2012)

## 2011
* Renuka Phadnis (Apr 9, 2011). "[Attempts on to revive Oriya Wikipedia". Mangalore](https://web.archive.org/web/20110414052442/http://www.hindu.com/2011/04/09/stories/2011040959272000.htm)". The Hindu. Retrieved June 20, 2012.
* "[Help: This Is A Stub](https://magazine.outlookindia.com/story/help-this-is-a-stub/272101)". Outlook. (June 13, 2011)

<a href="publications" class="button button--small">✍︎ publications</a> <a href="blog" class="button button--small">✐ blogs</a>
