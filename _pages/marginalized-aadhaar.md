---
title: MarginalizedAadhaar
subtitle: 2019 documentary film (69 min / color)
description: 2019 documentary film
featured_image:
permalink: marginalized-aadhaar
---

My 2019 documentary feature film _MarginalizedAadhaar_ explores the exclusion by a number of marginalised communities due to the rollout of India's national biometric ID program — Aadhaar.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/1QKt05JW7MU?start=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Released in 2021, this film was an outcome of the Digital Identity Fellowship I was awarded by Yoti in 2019. [Premiered](https://re-publica.tv/en/sideevent/marginalizedaadhaar-documentary-film-screening) at the re:publica 2021 Berlin where I was also invited for a [talk](https://re-publica.tv/en/session/no-digital-rights-if-you-marginalized-india), screened worldwide and included in academic courses and other studies.

# Reviews

> “This film shows the real, serious challenges of implementing Aadhar in India's heterogeneous and profoundly unequal society. While this socio-technical system promises to improve citizens' lives, it further marginalizes already vulnerable communities while raising profound privacy and security concerns for everyone. I highly recommend this film for anyone concerned about the growing role technology is playing in our everyday lives.”

<p style="text-align: center;">— SHOBITA PARTHASARATHY, PhD, University of Michigan</p>

> “This documentary by Subhashish focuses on issues of exclusion of marginalised communities.”

<p style="text-align: center;">— ENDANGERED LANGUAGES ARCHIVE</p>

> “This Film is flipping brilliant! Your film eloquently distills the pros and cons of biometric digital ID Aadhaar featuring expertise of Savita Bailur, Sunil Abraham and others.”

<p style="text-align: center;">— TONY ROBERTS, Research Fellow, Institute for Development Studies, University of Sussex, UK</p>

> “The importance of understanding the conditions of surveillance and citizens’ rights is vividly brought to light in Subhashish Panigrahi’s provocative film Marginalized Aadhaar. Through Panigrahi’s work, we learn that the effects of surveillance — no matter how well intended — can be uneven and often discriminatory.”

<p style="text-align: center;">— KANDREA WADE, ALEX TAYLOR, DANIELA ROSNER, MIKAEL WIBERG, ACM Interactions</p>

> “Excellent documentary by Subhashish Panigrahi, looking at India's biometric data collection programme and its negative impact on the Aadhaar community.”

<p style="text-align: center;">— RE:PUBLICA 2021</p>

> “In India, we chose Subhashish who is a particularly talented documentary filmmaker and researcher with a long history and track record in documenting marginalization. During his fellowship, he was looking at how the rollout of Aadhaar has caused marginalisation among communities in India, with registration and using it, highlighted in this revealing documentary.”

<p style="text-align: center;">— KEN BANKS, Former Head, Social Impact, Yoti</p>

# Links
* [Watch](https://www.youtube.com/watch?v=1QKt05JW7MU)
* [Report](https://www.yoti.com/wp-content/uploads/Marginalized_Aadhaar-Subhashish-Panigrahi.pdf), [Paper](https://interactions.acm.org/archive/view/march-april-2022/marginalizedaadhaar) on ACM Interactions and Global Voices blog series — [1](https://globalvoices.org/2020/02/17/marginalizedaadhaar-is-indias-aadhaar-enabling-more-exclusion-in-social-welfare-for-marginalized-communities/), [2](https://globalvoices.org/2020/06/29/marginalizedaadhaar-digital-identity-in-the-time-of-covid-19/), [3](https://globalvoices.org/2020/08/16/is-indias-digital-id-system-aadhaar-a-tech-solution-for-a-socio-economic-problem/)
* Covered in podcast India: Exclusion by Design ([Ep. 5](https://id169.com/india-exclusion-by-design-ep-5))
