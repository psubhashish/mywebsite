---
title: Films directed/produced
subtitle: National Geographic Society-awarded documentary filmmaker and researcher.
description: Featured list of films directed/produced
featured_image: '/images/main/'
---

[Reach out](/contact) for advisory and consultating on impact filmmaking and documentary research using Open Filmmaking framework (openness, community collaboration, diversity, ethics and inclusion).

## Bringing Down the Mountain (2023)
**14 min** / **color** / **[website](https://theofdn.org/film/bringing-down-a-mountain/)**

<img src="/images/page-post/film/BDAM_poster-980x449.jpg" alt="Theatrical poster of film Bringing Down the Mountain" height="172">

_A rural village never wanted to be a city’s landfill or a distant blur in Reels but rather home to new imaginations_.

This film intersects the contrasting themes of access, caste abolition in rural Indian village and a hyper-urban city, underlining that all digital barriers are social and exposing the plurality, extractivism and ableism in tech innovations. Made through the Interledger Foundation's Future I Money grant, this speculative film was premiered in November 2023 in Costa Rica receiving accolades and coverage, including one by the Blackmagic Design. A DVD of this film is archived at the US Library of Congress.

<hr>

## Gyani Maiya (2019)
**25 min** / **color** / **[website](https://theofdn.org/film/gyani-maiya/)**

<iframe src="https://player.vimeo.com/video/439168756?h=0b8eaf4aae&byline=0&portrait=0" width="640" height="293" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

>“Sensitive and heart-touching! save your language to save your soul.”

<p style="text-align: center;">— Anvita Abbi, Linguist</p>

>“Preserves and immortalizes a language on the brink of extinction.”

<p style="text-align: center;">— Ramil Mercado, Asia Matters for America</p>

_Adults in an Nepali indigenous community have long forgotten their language. How would the eldest of two last living speakers keep their near extinct language alive_?

Inspired by Global Voices [reportage](https://globalvoices.org/2017/08/16/indigenous-nepali-language-with-only-two-fluent-speakers-sees-pages-of-hope-in-newly-launched-dictionary/) by Sanjib Chaudhary, _Gyani Maiya_ was made in collaboration with Chaudhary, Nepalese researcher and lexicographer Uday Raj Aaley, and the late Gyani Maiya Sen-Kusunda (film protagonist and narrator), the last presumed speaker of the then near-extinct Kusunda language, with a National Geographic Society grant as a part of [OpenSpeaks](https://theofdn.org/openspeaks). Screenings worldwide and nine international wins (Best Screenplay, International Film & Entertainment Festival Australia (IFEFA) 2022, 6th International Folklore Film Festival 2023, Thrissur (India), Khamrubu International Short Film Festival 2023, Kamrup (India), Nepal America International Film Festival 2023, International Conference on Language Documentation & Conservation at University of Hawaiʻi (US), Lanham (US), Balkan Can Kino, Athens (Greece), Bozok Film Festivali, Yozgat (Turkey), Festival RNAB, Sao Paulo (Brazil) and screened at Lost Origins Gallery, Washington DC and dWeb Camp, Navarro CA (US)).

<hr>

## MarginalizedAadhaar (2021)
**69 min** / **color** / **[official page](/marginalized-aadhaar)**

<iframe width="560" height="315" src="https://www.youtube.com/embed/1QKt05JW7MU?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

> “Excellent documentary by Subhashish Panigrahi, looking at India's biometric data collection programme and its negative impact on the Aadhaar (_sic_) community.”

<p style="text-align: center;">— re:publica 2021</p>

_By 2021, India made over a billion people to enrol for the national biometric digital ID—Aadhaar. But what was the cost the marginalised had to bear_?

An outcome of extensive filming in India and abroad as a part of [Digital Identity Fellowship](https://www.yoti.com/blog/yoti-digital-identity-fellows/) awarded by Yoti in 2019, this 2021 film dissects exclusions by Aadhaar and how those could have been stopped. Premiered at the [re:publica 2021](https://re-publica.tv/en/sideevent/marginalizedaadhaar-documentary-film-screening) Berlin, screened worldwide and included in academic courses.

<hr>

## Nani Ma (2022)
**35 min** / **color** / **[official site](https://theofdn.org/film/nanima)**

<iframe width="560" height="315" src="https://www.youtube.com/embed/swYkai9c50c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

> “Though issues of language and culture are politically polarising, Panigrahi’s choice of human subject, his grandmother, seems more personal than political.”

<p style="text-align: center;">— Atri Prasad Rout, Orissa Post</p>

_As the 95-year-old Musamoni Panigrahi battles loss of memory, her never-before recorded songs and stories are in danger of being lost forever_.

First-ever documentary in the Baleswari dialect of Odia in which long-lost songs and stories come to life. Made a mark at Eighth International Folklore Film Festival 2025.

<hr>

## The Volunteer Archivists (2022)
**35 min** / **color** / **[official site](https://theofdn.org/film/tva)**
<iframe width="560" height="315" src="https://www.youtube.com/embed/4_j-2d0Ju24?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

> “A wonderful testament to the power of citizen archivists.”

<p style="text-align: center;">— Carl Malamud, Public Resource</p>

_The rarest publications in the Odia language dating back to early 1800s are perishing fast thanks to collective negligence. How does a group of volunteers save them digitally_?

A film about a volunteer archivist group in India and its 15-year-long struggle that saved some the rarest and earliest historical publications. International premiere in Singapore at Wikimania—the annual conference of Wikipedia.

<hr>

## Mage Porob (2019)
**35 min** / **color** / **[website](https://theofdn.org/film/mage-porob/)**

<iframe src="https://player.vimeo.com/video/439161153?h=fb09f8925b&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

_Everyone must come together to celebrate the harvest and receive blessings of the deities—the forest and the mountain_.

Second film of the National Geographic Society grant series, documenting the eponymous harvest festival of Mage Porob, the Ho people, an Indigenous tribe of eastern India. Made in colllaboration with the Veer Birsa Munda Ho Students Union Odisha (Birbasa).
<hr>

## Remosam (2019)
**35 min** / **color** / **[website](https://theofdn.org/film/remosam/)**

<img src="/images/page-post/Remosam_poster-980x449.jpg" alt="Theatrical poster of film Remosam" height="172">

_Far up on the Bonda Hills, the Bonda people are met with dysfunctional governance that fails to feed or keep their language alive_.

Third film of the National Geographic Society grant series, _Remosam_ is a linguistic and cultural documentation of the Indigenous and endangered Bonda language, and an outcome of collaboration with the upper Bonda community of Bonda Hills, lexicographer Gobardhan Panda, the Council of Analytical Tribal Studies, and scholar Prateek Pattnaik (co-cinematographer). Premiered at the 2018 Koraput Literary Festival.

<hr>

[Reach out](/contact) for screening of these films or for reviews and interviews.

<hr>

## Filmmaker Bio

Subhashish Panigrahi is an award-winning documentary filmmaker whose films have won many international awards, grants and accolades. His films depict plural and complex societies about language, tech and media and are often used as language materials by native speakers. He has ten films and other media works to his credit. In 2017, he founded OpenSpeaks, a lab to help digital language activists build media and tech resources for low- and medium-resourced languages, earning the distinction of National Geographic Explorer. 2021 documentary MarginalizedAadhaar and the Interledger Foundation awarded him the prestigious Future⎮Money Grant, helping him make his maiden docu-fiction, Bringing Down a Mountain.

His films are deeply personal and political, capturing the narratives and worldviews of those whose stories and languages are portrayed in those films. His 2021 film Gyani Maiya portrays the story of an elder whose language—Kusunda—was at the brink of extinction; MarginalizedAadhaar (2021) dares to ask, “whose tech is it anyway that marginalises the ones that are already marginalised?”; Bringing Down a Mountain (2023) reimagines a future of inclusive digital payments; he volunteered his labour to make The Volunteer Archivists (2020), which portrays the monumental digital archive Odia Bibhaba; and in Nani Ma (2022), he brought to life the sing-song poems in Baleswari, a dialect of Odia that is rarely documented in audiovisual media.
