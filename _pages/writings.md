---
title: Select Writings and Media Coverage
subtitle:
description: Select Writings and Media Coverage
featured_image:
---

[ORCID](https://orcid.org/0000-0002-3665-8184) • [Google Scholar](https://scholar.google.com/citations?user=vQ0kymEAAAAJ&hl=en)

## Book chapters
* “Can Openness and Open Standards Help Revitalise Marginalised Languages?”. _Young People, Social Inclusion and Digitalisation_, Council of Europe, 2021, p. 73, https://pjp-eu.coe.int/documents/42128013/47261623/YKB-27-WEB.pdf/dbab979b-75ff-4ee8-b3da-c10dc57650d5.
* "Rising Voices. Indigenous Language Digital Activism". _Digital Activism in Asia Reader_. Lüneburg: meson press, 2015. https://doi.org/10.25969/MEDIAREP/1351.
* “OpenSpeaks: Transforming Learning of Tech Innovations in Low-Resource Language Documentations to Open Educational Resources.” Linguapax Review 2021, vol. 9, 2021, pp. 124–37, https://www.linguapax.org/wp-content/uploads/2022/02/LinguapaxReview9-2021-low.pdf.

## Academic/Research
* “[Enhancing Discoverability, Searchability and Citability through Re-Archiving Odia-language Texts](https://wikiworkshop.org/papers/enhancing-discoverability-searchability-and-citability-through-re-archiving-odia-language-texts.pdf)”. Wiki Workshop 2024
* “[From Erasure to Documentation: Authority Control Data for Indian Artists and Artisans](https://wikiworkshop.org/papers/from-erasure-to-documentation-authority-control-data-for-indian-artists-and-artisans.pdf)”. Wiki Workshop 2024
* “Reclaiming Our Voices: Imagining Community-Led AI/ML Practices”. 2nd Annual Meeting of the ELRA/ISCA SIG on Under-resourced Languages (SIGUL 2023), Dublin, Ireland. ISCA; 2023. p. 1–3. https://dx.doi.org/10.21437/SIGUL.2023-1.
* “OpenSpeaks before AI: Frameworks for Creating the AI/ML Building Blocks for Low-Resource Languages.” Interactions, vol. 30, no. 3, May 2023, pp. 6–7. ACM Digital Library, https://doi.org/10.1145/3591211.
* "Building a Public Domain Voice Database for Odia".In Companion Proceedings of the Web Conference 2022 (WWW '22). Association for Computing Machinery, New York, NY, USA, 1331–1338. https://doi.org/10.1145/3487553.3524931
* “MarginalizedAadhaar: India’s Aadhaar Biometric ID and Mass Surveillance.” Interactions, vol. 29, no. 2, Feb. 2022, pp. 16–19. https://doi.org/10.1145/3517173.
* “Open Civic Engagement in a Panopticon State.” Interactions, vol. 28, no. 6, Nov. 2021, pp. 10–11, https://doi.org/10.1145/3494046. (_as a part of the [Otherwise School](https://sites.uw.edu/otherwise/) at the University of Washington_)
* “An Account of How Critical Information about Privacy Is Missing in Indigneous Languages of India and Ways to Avoid It.” 7th International Conference on Language Documentation and Conservation (ICLDC), University of Hawai’i, Manoa Hamilton Library, 2021, p. 6, https://doi.org/10125/74513.
* Panigrahi, Subhashish, and Eddie Avila. “Building Networks of Language Digital Activists for Peer Learning and Knowledge Exchange.” 7th International Conference on Language Documentation and Conservation (ICLDC), University of Hawai’i, Mānoa Hamilton Library, https://doi.org/10125/74476.
* Panigrahi, Subhashish, and Saxena, Pooja. Bringing Ol Chiki to the Digital World. Humanities Commons, 2016, https://doi.org/10.17613/FXTH-8555.

## Essays
Author of over a few hundred essays, op-eds and blogs in several noted publications including [Global Voices](https://globalvoices.org/author/psubhashish/), [Opensource.com](https://opensource.com/users/psubhashish), [The Wire](https://thewire.in/author/s-p), HuffPost, [Wikimedia Blog](https://diff.wikimedia.org/author/psubhashish/), [ Mozilla](https://blog.mozilla.org/community/2014/07/08/mozilla-brings-indian-communities-together-twice-in-one-month/), [Creative Commons](https://creativecommons.org/2014/04/18/report-from-india-relicensing-books-under-cc/), [Asia Times](https://asiatimes.com/author/subhashish-panigrahi/), [The Hoot](http://asu.thehoot.org/search/subhashish~panigrahi), Medianama and [DNA](https://web.archive.org/web/20180407003138/http://www.dnaindia.com/authors/subhashish-panigrahi) and [Bangalore Mirror](https://bangaloremirror.indiatimes.com/getsearchdata.cms?query=Subhashish+Panigrahi).

Select recent essays:
* Panigrahi, Subhashish. “Is Digital ID System, Aadhaar, a Tech Solution for Socio-Economic Problem?” Business Standard India, 18 Aug. 2020. Business Standard, https://www.business-standard.com/article/current-affairs/is-digital-id-system-aadhaar-a-tech-solution-for-socio-economic-problem-120081800243_1.html.
* Panigrahi, Subhashish. “There Are 23 Indian-Language Wikipedias. The Oldest Just Turned 15.” Business Standard India, 9 June 2017. Business Standard, https://www.business-standard.com/article/current-affairs/there-are-23-indian-language-wikipedias-the-oldest-just-turned-15-117060900589_1.html.
* Panigrahi, Subhashish. “8 Challenges In Growing Indian-Language Wikipedias.” HuffPost, 19 Mar. 2016, https://www.huffpost.com/archive/in/entry/8-challenges-in-growing-indian-language-wikipedias_b_9457704.
* Panigrahi, Subhashish.“Why It’s Essential To Grow Indian-Language Wikipedias.” HuffPost, 25 Jan. 2016, https://www.huffpost.com/archive/in/entry/why-its-essential-to-grow-indian-language-wikipedias_b_9025690.

## Recognitions
* Opensource.com: People's Choice Award (2015) and Wikimedia Advocate and Expert (2016)

## Art and research residencies
* 2023 — [DWeb Camp](https://dwebcamp.org/), Internet Archive, California, US.
* 2021 — [Otherwise School](https://sites.uw.edu/otherwise/), University of Washington, Seattle WA, US (see [publication](https://doi.org/10.1145/3494046)).
* 2021: "[Understanding Web Content Monetization in the Ho and Santali Languages](https://meta.wikimedia.org/wiki/Research:Understanding_Web_Content_Monetization_in_the_Ho_and_Santali_Languages)", OpenSpeaks, Grant for the Web.
* 2019: Collaboration with artist Paribartana Mohanty for the performance "[What does it take to imagine a resistance in fiction?](https://paribartanamohanty.wordpress.com/2020/06/26/1863/)" [Five Million Incidents](https://www.goethe.de/ins/in/en/kul/art/fmi/pam.html) (2019-2020) with support by Goethe-Institut/Max Mueller Bhavan and Raqs Media, New Delhi, India.

## Select Media Coverage
* "[Docufiction Film Bringing Down a Mountain Edited with DaVinci Resolve Studio](https://www.blackmagicdesign.com/media/release/20240315-01)." Blackmagic Design (2024).
* Rout, Atri Prasad. “[Nani Ma: A Defence of Diversity](https://www.orissapost.com/atri-nani-ma-a-defence-of-diversity/).” OrissaPOST (2023).
* Mishra, Dhanada K. “[The Volunteer Archivist](http://odishapostepaper.com/m/200324/643703bba60b2).” OrissaPOST (2023).
* Bellini, Priscila, et al. “[Subhashish Panigrahi and Meaningful Access to the Internet in South Asia and Beyond](https://podcast.whoseknowledge.org/posts/ep13/).” Whose Voices (2023).
* Verma, Swati. “[Nani Ma Review](https://www.ukfilmreview.co.uk/reviews/nani-ma).” UK Film Review (2023).
* Cherry, Rebecca. “[Review: Nani Ma](https://filmcarnage.com/2023/03/28/review-nani-ma).” Film Carnage (2023).
* Mercado, Ramil. “[Speaker of Endangered Kusunda Language Heard in Washington, DC](https://asiamattersforamerica.org/articles/speaker-of-endangered-kusunda-language-heard-in-washington-dc).” Asia Matters for America (2023).
* Franz, Benjamin. “[Nani Ma Featured, Reviews Film Threat](https://filmthreat.com/reviews/nani-ma/).” Film Threat (2022).
* Hersey, Frank. “[India: Exclusion by Design (Ep. 5) / ID16.9 Legal Identity Podcast](https://id169.com/india-exclusion-by-design-ep-5).” ID16.9, Biometric Update (2022).
* Brookes, Tim. “[The Volunteer Odia Archivists – Endangered Alphabets](https://www.endangeredalphabets.com/2022/11/16/the-volunteer-odia-archivists/).” Endangered Alphabets, 16 Nov. 2022.
* Vidal, Laura. “[Subhashish Panigrahi by Sounds of Mozilla’s 2020 Internet Health Report](https://podcasters.spotify.com/pod/show/2020internethealthreport/episodes/Subhashish-Panigrahi-es0bk3).” Mozilla (2021).
* Jones, Andy. “[Is the UK Ready for the Future of Digital ID?](https://www.raconteur.net/digital-transformation/is-the-uk-ready-for-the-future-of-digital-id)” Raconteur (2021).
* Vidal, Laura. “[Subhashish Panigrahi — The Internet Health Report 2020](https://2020.internethealthreport.org/people/subhashish/).” Internet Health Report 2020, Mozilla (2020).
* Benjakob, Omer. “[A Vicious Culture War Is Tearing through Wikipedia](https://www.wired.co.uk/article/wikipedia-culture-war).” Wired UK (2020).
* James, Christie Maria. “[Meet Subhashish Panigrahi, the Host of the @AsiaLangsOnline Twitter Account for August 6-12](https://rising.globalvoices.org/blog/2019/08/05/subhashish-panigrahi/).” Rising Voices, (2019).
* Rajpal, Seema. “[O Foundation and National Geographic Set out to Document Endangered Languages before They Are Lost Forever](https://www.edexlive.com/happening/2018/aug/25/o-foundation-and-national-geographic-set-out-to-document-endangered-languages-before-they-are-lost-f-3728.html).” Edex Live, The New Indian Express (2018).
* Huger, Jen Wike. "[Cultural Knowledge Needs to Be More Open](https://opensource.com/community/15/2/contributor-spotlight-subhashish-panigrahi)". Opensource.Com (2015).
