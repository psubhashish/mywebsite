---
title: Public speaking
subtitle: Keynotes and other public talks, lectures and workshops
description: Public speaking
featured_image:
---

I have been invited to deliver keynotes, talks, and conduct workshops and panels in reputed conferences worldwide, including a keynote at the SIGUL 2023 (Interspeech '23) in Dublin, Ireland, a keynote at Global Voices Citizen Media Summit 2019 in Taipei, Taiwan, an invited talk at the UNESCO International Conference in Changsha, China, several United Nations Internet Governance Forums, two TEDx talks, a digital language activism workshop series by Rising Voices, and an invited talk at OER 16 in Edinburgh, Scotland.

<div class="gallery" data-columns="3">
    <img src="/images/page-post/talk-p1.gif">
    <img src="/images/page-post/talk-l1.gif">
    <img src="/images/page-post/talk-p2.gif">
    <img src="/images/page-post/talk-l2.gif">
</div>

## Long list
### 2024
* (upcoming) Talk at LD&A 2024: Language Documentation and Archiving 2024 Berlin (Germany) — "Building Foundational Language Data with A Community-First Approach"
* (upcoming) Paper presentation at [Wiki Workshop](https://wikiworkshop.org/2024/) — "From Erasure to Documentation: Authority Control Data for Indian Artists and Artisans" and "Enhancing Discoverability, Searchability and Citability through Re-Archiving Odia-language Texts"

### 2023
* Poster presentation at Creative Commons Global Summit, Mexico City, Mexico — "[OpenSpeaks Voice: Building South Asia's largest speech data repository under Public Domain](https://site.pheedloop.com/event/EVEWHNCWWDNEG/schedule/SESYGLBYOPYLAPMP5)"
* Keynote at SIGUL (Special Interest Group on Under-resourced Languages) 2023 at Interspeech, Dublin, Ireland — "[Reclaiming Our Voices: Imagining Community-Led Ai/Ml Practices](https://sigul-2023.ilc.cnr.it/programme/)" (paper [DOI: 10.21437/SIGUL.2023-1](https://dx.doi.org/10.21437/SIGUL.2023-1))
* Panel at PULiiMA, Darwin, Australia — "[The emerging movement of language digital activism](https://puliima.com/)"
* Screening of film _The Volunteer Archivists_ and Talk at Wikimania 2023, Singapore
* Explorer Takeover (workshop) at 2023 National Geographic Explorers Festival — "Open Filmmaking"
* Screening of film _Gyani Maiya_, _The Volunteer Archivists_ and talk at Decentralized Web Camp (DWeb Camp) 2023, Camp Navarro, California, US
* Panel at RightsCon 2023, Costa Rica
* Talks at MozFest 2023
* Screening of film _The Volunteer Archivists_ at Samadrusti, Bhubaneswar, India
* Screening of films _The Volunteer Archivists_ and _Nani Ma_ at Bakul Foundation, Bhubaneswar
* Talk at WikiConference India 2023
* Panel at digitALL: Access To Rights & Rights To Access at Bengaluru, India
* Film screening and talk at Lost Origins Gallery — [Gyani Maiya](https://lostorigins.gallery/gallery-events/gyani-maiya-screening/)([review](https://asiamattersforamerica.org/articles/speaker-of-endangered-kusunda-language-heard-in-washington-dc))
* Collaborated at National Geographic Society Storytellers Summit 2023, Washington D.C.

### 2022
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/IOEnFNol-LY?start=4065" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
* Talk at Language Documentation and Archiving conference 2022, Berlin — ["Bridging the Divide: Connecting Language Activist Efforts and Language Archives"](https://langdoc.org/conference/), [Recording](https://www.youtube.com/watch?v=IOEnFNol-LY&list=PLLsMR5ejD4iS5XmEvcdoBTAmGBe7XxMwY&index=24)
* Panel at [El activismo digital en favor de las lenguas en otros rincones del mundo: Oportunidades para los intercambios transregionales](http://ccemx.org/evento/foro-internacional-lenguas-indigenas-e-innovacion-filii/), Mexico City
* Poster & paper presentations at Wiki Workshop 2022, The Web Conference 2022 — ["Building a Public Domain Voice Database for Odia"](https://www2022.thewebconf.org/accepted-workshop-papers/)
* Art Exhibition Mighty Margins, Ashoka in Bengaluru, India
* Panel at [Linguapax International](https://www.eventbrite.es/e/language-technologies-and-language-diversity-registration-262442862667)
* Talks at Mozilla Festival — ["Low-resource languages, and their open source AI/ML solutions through a radical empathy lens"](https://pretalx.com/mozfest-2022/talk/review/EZY97GMNEH9Y3SLWEYV8GAZ37XK98G3M) ([archive](https://schedule.mozillafestival.org/session/USV9PC-1)); Panel — ["Transcribing Oral Knowledge with Wikisource](https://pretalx.com/mozfest-2022/talk/review/KLDNLP8ADRK9BTXUQPYBAQDG9WVRNLBV) ([archive](https://schedule.mozillafestival.org/session/UTEJE3-1)); Panel — ["With what words should I speak? Impact of Voice technology on Language Diversity"](https://pretalx.com/mozfest-2022/talk/review/LRWXNRZUN7MTKEV7XHAZBTM9MYHAR87D) ([archive](https://schedule.mozillafestival.org/session/ZHY8VH-1))

### 2021
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/5gzcH1tIjqw?start=4065" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
* Talk at  Internet Governance Forum, United Nations — "[Building the Wiki-Way for Low-Resource Languages](https://intgovforum.org/en/content/igf-2021-town-hall-42-building-the-wiki-way-for-low-resource-languages)
* Co-facilitated [Language Digital Activism Workshops for India](https://rising.globalvoices.org/language-digital-activism-workshops-for-india/)
* Panel at Othering & Belonging Conference 2021. UC Berkely — ["Reimagining the Politics of Technology Creation: Towards Technology for Belonging"](https://belonging.berkeley.edu/2021-othering-belonging-conference/reimagining-politics-technology-creation-towards-technology)
* Panel at Response-ability Tech 2021 Summit — ["National biometric identity systems: The case of Aadhaar"](https://response-ability.tech/2021-conference/) ([video recording](https://vimeo.com/563652546))
* Talk at RightsCon 2021 — "Building an open toolkit to document Indigenous languages"
* Featured Talk at Re:Publica — "[No Digital Rights if You Are Marginalized in India](https://re-publica.tv/en/session/no-digital-rights-if-you-marginalized-india)". [video](https://www.youtube.com/watch?v=5gzcH1tIjqw), and International Premiere of film [MarginalizedAadhaar](https://re-publica.tv/sideevent/marginalizedaadhaar-documentary-film-screening)
* Talk at Asia Indigenous Youth Platform Consultation, UNESCO Bangkok — "@AsiaLangsOnline campaign and Rising Voices"
* Talks at Mozilla Festival 2021 — ["OpenSpeaks: Translating language documentation toolkit into an indigenous language"](https://schedule.mozillafestival.org/session/C39C9N-1), ["Multilingual films as digital rights teaching aids for activism in Asia-Pacific"](https://schedule.mozillafestival.org/session/F3DESS-1)
* Presentations at at [7th International Conference on Language Documentation & Conservation](ling.lll.hawaii.edu/sites/icldc/) (ICLDC). University of Hawai‘i at Mānoa — ["An account of how critical information about privacy is missing in indigenous languages of India and ways to avoid it"](https://icldc7.sched.com/event/hgyS/paper-qa-session-23) (poster) ["Building networks of language digital activists for peer learning and knowledge exchange"](https://icldc7.sched.com/event/hh0y/poster-qa-session-22) (poster, with Eddie Avila)
* Workshop ["Story Time Cafe with Internet Master Storytellers by Mozilla"](https://internetfreedomfestival.org/wiki/index.php/Story_Time_Cafe_with_Internet_Master_Storytellers)
* Online Coursera MOOC for Linguapax, Universitat Autònoma de Barcelona — "[Rising Voices: Amplifying Marginalized Voices through Digital Language Activism](https://www.coursera.org/lecture/linguistic-diversity-what-for/video-lesson-QJYnA)" (a part of series "Linguistic Diversity, What for?")

### 2020
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/y4okn6rm9lY?start=4065" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
* Lecture at Sciences Po, Paris — "MarginalizedAadhaar: Aadhaar and exclusion of marginalised communities in India" (in course "[Aadhar: India's digital identity project for a billion people](http://formation.sciences-po.fr/enseignement/2020/KETU/2240), by Prateek Sibal)
* Panel at Internet Governance Forum (IGF) 2020 — ["Data to Inclusion: Building datasets in African Languages"](https://www.intgovforum.org/multilingual/content/igf-2020-ws-12220-data-to-inclusion-building-datasets-in-african-languages) ([recording](https://www.youtube.com/watch?v=y4okn6rm9lY))
* Talks at Creative Commons Virtual Global Summit — ["How social justice initiatives can further Creative Commons"](https://ccglobalsummit2020virtual.sched.com/#) (("[Reading](https://ccglobalsummit2020virtual.sched.com/event/ef4K)); and "Building content from bottom up"
* Collaboration at eLife Innovation Sprint 2020
* Panel at Celtic Knot Wikimedia Language Conference — ["COVID-19 and the Wikimedia community"](https://meta.wikimedia.org/wiki/Celtic_Knot_Conference_2020/Submissions/Panel_discussion:_COVID-19_and_the_Wikimedia_community)

### 2019
* Lecture at DEF Dialogues, Digital Empowerment Foundation — ["Digitally preserving marginalised languages"](https://www.facebook.com/339133342748/videos/462871697600300/)
* Keynote at Global Voices Asia Pacific Summit 2019 at Taipei, Taiwan — ["The rise and challenge of digital authoritarianism in the Asia Pacific"](https://asiasummit2019.globalvoices.org/speakers/#Morning_Plenary_Session)
* Talk at Creative Commons Global Summit 2019, Lisbon, Portugal — ["OpenSpeaks: Open Archives of the Voices of the Commons"](https://ccglobalsummit2019lisbonportugal.sched.com/event/MnKe/openspeaks-open-archives-of-the-voices-of-the-commons)

### 2018
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2K9mxuxKdVQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
* Invited Talk at UNESCO International Conference "Role of Linguistic Diversity in Building a Global Community with Shared Future" was held in Changsha, China — ["Rising Voices: The Digital Possibilities for Marginalized Languages"](https://en.unesco.org/sites/default/files/china_conference_agenda_18sept2018.pdf)
* Presentation at National Geographic Citizen Science Workshop 2018, Hong Kong
* [Talks](https://theofdn.org/blogs/conference/wikimania2018/) at Wikimania 2018, Capetown, South Africa
* Talk at Nepal Internet Governance Forum (Nepal IGF) at Kathmandu, Nepal — ["Multi-stakeholder Model of Internet Governance Forum and Issues of Developing Countries"](https://drive.google.com/file/d/1VuecGMmEYp7darw0uQh8Xbw1919682K-/view)
* Organised Workshop Internet Society Southeast Asia Focused Engagement in Bangkok, Thailand
* Organised 2018 Internet Society Asia-Pacific & Middle East Chapters [Workshop](https://www.internetsociety.org/blog/2018/07/2018-internet-society-asia-pacific-middle-east-chapters-meeting/) in Kathmandu, Nepal

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/jkDVxos1sfk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
* Talk at  Celtic Knot Wikipedia Languages Conference 2018, The National Library of Wales, Aberystwyth, Wales, UK — ["How to create a digital archive for indigenous languages"](https://wikimedia.org.uk/wiki/Celtic_Knot_Conference_2018/Submissions/How_to_create_a_digital_archive_for_indigenous_languages)
* Organized 2018 Internet Society Stakeholders Meeting at Yangon, Myanmar

### 2017
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/kW4BQFZVgqY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
* Invited Talk at TEDxIBABangalore, Bengaluru, India — ["Digital collaboration to Save the Languages"](https://www.youtube.com/watch?v=kW4BQFZVgqY)
* Invited Talk at TEDxYouthAmaatraAcademy — ["Would you let a language die"](https://www.ted.com/talks/subhashish_panigrahi_would_you_let_a_language_die)
* Organised 2017 Internet Society Asia-Pacific Chapters Engagegement in Singapore

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/qqNwls8NsOk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
* Workship at Celtic Knot Conference 2017, University of Edinburgh UK — ["Kathabhidhana, open source toolkit to record pronunciations of any world language"](https://wikimedia.org.uk/wiki/Celtic_Knot_Conference_2017/Programme/CK129) (with Prateek Pattanaik, [recording](https://media.ed.ac.uk/playlist/dedicated/51020161/1_rneev7bo/1_2xo62fv1))
* Talk at Wikimania 2017, Montreal, Canada — ["Recording words for Wiktionary and preparing for an AI assistant"](https://wikimania2017.wikimedia.org/wiki/Submissions/Kathabhidhana:_Recording_words_for_Wiktionary_and_preparing_for_an_AI_assistant)
* Co-facilitated [Mozilla D&I Design Sprint 2017](https://wiki.mozilla.org/Diversity_and_Inclusion_Strategy_for_Participation/Design_Sprint), Vancouver, Canada

### 2016
* Facilitator at Mozilla All Hands 2016, Hawaii, US
* Collaborator at All Things Open 2016, Raleigh, NC, US
* Talk at FUEL GILT Conference 2016, New Delhi, India — ["Cross-community collaboration in Localization"](https://commons.wikimedia.org/wiki/File:Presentation_by_Subhashish_Panigrahi_at_the_FUEL_GILT_Conference_2016,_New_Delhi.webm)” ([recording](https://youtu.be/eJfnWodVvlo))
* Panel and workshop at WikiConference India 2016, Chandigarh, India — "How to better tell your Wikimedia community story using media as a tool"
