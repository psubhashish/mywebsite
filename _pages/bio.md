---
title: About
subtitle: 
description: 
featured_image: /images/demo/demo-landscape.jpg
---
<img src="/images/page-post/profile-landscape.jpg" alt="Profile picture" height="172">

Subhashish is presently the Law for All director at Ashoka, a board member at Global Voices, and a 2017-batch National Geographic Explorer.

In his 13 year-long career, Subhashish has led fundraising, grantmaking, community strategy, programmes, campaigns and events in the digital/human rights, open source, tech, media and multilingual internet spaces, serving 60% of the world's population. As a senior civil society leader, he has raised over $1M in grants, managed over $4M in programmes, made a wide number of grants, and has managed other leaders, overseeing large teams.

He has previously held leadership roles at Wikimedia, Mozilla, Internet Society and the Centre for Internet and Society. He has been a 2023 Internet Archive dWeb Fellow, 2023 Future⎮Money Artist, 2019 Yoti Digital Identity Fellow and a 2017 MJ Bear Fellow. He was a keynote speaker at the Special Interest Group on Under-resourced Languages (SIGUL ‘23, Dublin); he has spoken widely at many noted international conferences, including UNESCO and UN deliberations, in over 20 countries. Subhashish is well well-published, contributing to three books, one being an EU publication, and, the other, a UNESCO toolkit for digital language activism, apart from other journal, print and web publications.

As an award-winning documentary filmmaker, his films have won a National Geographic Society grant, a Future⎮Money artist grant, and a Yoti Digital Identity Fellowship, and have been screened around the world, winning accolades.

He activated numerous social entrepreneurs who are Ashoka Fellows with continental impact; catalysed the growth of 40K open internet leaders in 40 Asia-Pacific economies; kickstarted community, media, and participatory research in 10 low-resource languages; provided strategic support to 23 Wikipedia communities and university partners; leading to significant growth in student editors, Wikipedia content, participation increase; and built Open Educational Resources (OER) in seven languages; and designed multi-faceted research for Mozilla Asia Campus Clubs.

## Art and [filmmaking](/films)
Subhashish has directed ten documentaries. Among his most acclaimed films are *MarginalizedAadhaar* (2021), which premiered at re:publica 2021 Berlin and discussed in University of Michigan and Sciences Po Paris courses, and conferences Response-ability Tech 2021 and ICLDC University of Hawai‘i, and *Gyani Maiya* (2019) which won the 2022 International Film & Entertainment Festival Australia (IFEFA) Best Screenplay-Documentary Award, Nepal America International Film Festival (NAIFF), sixth International Folklore Film Festival 2023, Balkan Can Kino Athens 2022, Bozok Film Festivali 2022, Festival RNAB 2021 Sao Paulo and Khamrubu International Short Film Festival 2023. His films have been screened in multiple US cities, Brazil, Germany, Italy, Singapore, Turkey, and in his home country India. Several of these films are at the Library of Congress headquarters in Washington DC.

## Open source and language tech
Since 2006, Subhashish has been a leader in building a multilingual internet, a space where linguistic and cultural diversity is celebrated, and not subjugated. He spearheaded a campaign in 2011 that revived the eight-year-dormant Odia-language Wikipedia. In 2017, he founded OpenSpeaks, a lab for digital language activists to document low- and medium-resourced languages. He has largely contributed to research on structural barriers in multilingual societies and built language tech and other resources.

Subhashish has created over 5K Wikipedia articles. In 2023, he created the largest openly licensed speech corpus (72K recordings) in his native language, Odia, which was also the largest South Asian-language speech data under a universal Public Domain release. He has further contributed 4K recordings of sentences in Odia on Mozilla's Common Voice, the highest contribution in the language. He has led numerous open-source projects, creating transliteration schemes, typefaces and input methods in multiple languages/writing systems.

>“This pioneering work announces future efforts, with ‘regional’ oralities of India and the world rising back to claim their place in the digital landscape.”
<p style="text-align: center;">— Hugo Lopez, Co-founder, Lingua Libre</p>

## Research
Focusing on intersectional and structural issues in the tech, society, language and media spaces, Subhashish has designed multiple research projects. He received an award from Creative Commons for expanding OpenSpeaks in Santali, an Indigenous language from India; his study of Odia-language users’ digital safety and security, awarded by Rising Voices, contributed to the noted UNESCO toolkit “Digital initiatives for Indigenous languages”; and he led an investigation on web content monetisation in Ho and Santali languages, winning a Grant for the Web award. He has unveiled his research findings in numerous conferences, journals and other [publications](/writings). 

## Academia
Early in his career (2012–2016) at the Wikimedia Foundation and the Centre of Internet Society, Subhashish designed and implemented the Wikipedia Education Program across India by collaborating with noted universities and training educators and student ambassadors. Later, he has taught and designed courses across noted institutions—Christ University, Srishti, Sciences Po, and Indian Institute of Technology Madras, to name a few. He has spoken in multiple academic conferences including a 2023 keynote at SIGUL (Special Interest Group on Under-resourced Languages) 2023 at Interspeech, Dublin, Ireland.

## Fellowships and awards
* [2023 Decentralized Web Fellowship](https://blog.archive.org/2023/08/11/dweb-fellows-2023-lighting-the-path-towards-a-better-web/), Internet Archive
* [Digital Security + Language](https://rising.globalvoices.org/digital-security-language/), Rising Voices
* [2021 Otherwise School](https://sites.uw.edu/otherwise), University of Washington
* Grant for the Web
* 2020 Global Network Communities Activity Fund, Creative Commons
* [2019 Digital Identity Fellowship](https://www.yoti.com/blog/yoti-digital-identity-fellows/), Yoti
* [2017 National Geographic Explorer](https://explorer-directory.nationalgeographic.org/subhashish-panigrahi)
* [2017 MJ Bear Fellowship](https://journalists.org/profiles/subhashish-panigrahi/), Online News Association
* [2015 Opensource.com People's Choice Award](https://opensource.com/community/15/2/contributor-spotlight-subhashish-panigrahi)
