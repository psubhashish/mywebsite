---
title: Advisory and Consulting
subtitle: 
description: I help organizations and changemakers scale deep and change systems.
featured_image: '/images/main/'
---
<!-- Central idea starts -->
<style>
  .image-container {
    position: relative;
    text-align: center;
  }

  .overlay-layer {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(1, 7, 27, 0.7); /* Black with 10% opacity */
    z-index: 1; /* Ensure the layer is above the image but below the text */
  }

  .overlay-text {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    font-size: 2em; /* Adjust font size as needed */
    font-weight: bold;
    background-color: rgba(0, 0, 0, 0); /* Black background with 80% opacity */
    color: #f9c312; /* Slightly dull yellow color */
    padding: 10px; /* Add some padding around the text */
    z-index: 2; /* Ensure text is above the overlay layer */
  }

  .image-container img {
    width: 100%; /* Ensure the image is responsive */
    height: auto;
    display: block;
  }

  @media only screen and (max-width: 600px) {
    .overlay-text {
      font-size: 1.5em; /* Adjust font size for mobile view */
      padding: 5px; /* Adjust padding for mobile view */
    }
  }
</style>

<div class="image-container">
  <img src="/images/page-post/svc-lede.gif" alt="services lede image">
  <div class="overlay-layer"></div>
  <div class="overlay-text">I help organizations and changemakers scale deep and change systems.</div>
</div>
<!-- Central idea ends -->
<!--Index starts-->
<!--<p><center>
[Non-profit strategy](#non-profit-strategy) • [For-profit giving and social strategy](#for-profit-giving-and-social-strategy) • [AI and data](#ai-and-data) • [Film and media-making](#film-and-media-making)
</center></p>-->
<!--Index ends-->
<br>
<br>
I help nonprofits and businesses graduate their new initiatives **from idea to project closure** stage through fundraising, prototyping, user testing, and implementation to evaluation. By guiding decision makers to **move through disagreements** and **identify your mission’s systemic barriers**, I help you co-lead strategic plans to remove your community's biggest barriers. As you wrap up projects, I can help you measure the **Intersectional Impact of Giving and Receiving** (IIGR) framework I created to measure your intervention’s impact on your community’s most vulnerable.

<section class="logos logo-section">
  <p><center><i>Organizations associated with</i></center></p>
  <style>
    .logo-section {
      text-align: center; /* Center-align the logos block */
      padding: 0 10%; /* Add horizontal padding for spacing */
    }

    .logo-container {
      display: flex;
      flex-wrap: wrap;
      justify-content: center;
      align-items: center;
      max-width: 1000px; /* Limit maximum width for responsiveness */
      margin: 0 auto; /* Center the container */
    }

    .logo {
      height: 45px;
      margin: 10px;
    }

    @media (max-width: 500px) {
      .logo {
        height: 30px; /* Adjust height for mobile devices */
      }
    }
  </style>
  <div class="logo-container">
    <img src="/images/page-post/logos/ashoka-logo.png" alt="Ashoka" class="logo">
    <img src="images/page-post/logos/isoc-logo.png" alt="Internet Society" class="logo">
    <img src="images/page-post/logos/mozilla-logo.png" alt="Mozilla" class="logo">
    <img src="images/page-post/logos/wmf-logo.png" alt="Wikimedia Foundation" class="logo">
    <img src="images/page-post/logos/ngs-logo.png" alt="National Geographic Society" class="logo">
    <img src="images/page-post/logos/cc-logo.png" alt="Creative Commons" class="logo">
    <img src="/images/page-post/logos/gv-logo.png" alt="Global Voices" class="logo">
    <img src="images/page-post/logos/cis-logo.png" alt="Centre for Internet and Society" class="logo">
    <img src="/images/page-post/logos/yoti-logo.png" alt="Yoti" class="logo">
    <img src="/images/page-post/logos/ona-logo.png" alt="Online News Association" class="logo">
    <img src="/images/page-post/logos/ek-type-logo.png" alt="Ek Type" class="logo">
    <img src="/images/page-post/logos/pratham-books-logo.png" alt="Pratham Books" class="logo">
    <!-- Add more logos here if needed -->
  </div>
  <hr>
</section>

Over 13 years, I’ve served over 60% of the world’s population through work at Ashoka, Wikimedia Foundation, Mozilla, Internet Society, and the Centre for Internet and Society. I consult and advise on three broader fields:

<br>
<div style="text-align: center;">
  <div style="border: 2px solid black; padding: 10px; display: inline-block; font-size: 2em;">
    <div style="display: inline-block; padding-right: 10px;">1.</div>
    <div style="display: inline-block; border-left: 2px solid black; padding-left: 10px;">Openness for equitable access and participation</div>
  </div>
</div>

Access and participation play a critical role in social justice. I can help you unleash the potential of open knowledge (Wikipedia, Wikidata, and Wikimedia projects), open source, standards and licenses to significantly increase access and participation.

<br>
<div style="text-align: center;">
  <div style="border: 2px solid black; padding: 10px; display: inline-block; font-size: 2em;">
    <div style="display: inline-block; padding-right: 10px;">2.</div>
    <div style="display: inline-block; border-left: 2px solid black; padding-left: 10px;">Digital and Human Rights</div>
  </div>
</div>

How can your work address growing digital and other human rights concerns, and inform the future course of social innovations? I work closely with each organization to measure impact against mission-aligned values, and help identify the systemic digital and human rights barriers. 

<br>
<div style="text-align: center;">
  <div style="border: 2px solid black; padding: 10px; display: inline-block; font-size: 2em;">
    <div style="display: inline-block; padding-right: 10px;">3.</div>
    <div style="display: inline-block; border-left: 2px solid black; padding-left: 10px;">Community-based Data and Media</div>
  </div>
</div>

How can communities access, disseminate and own knowledge meaningfully and effectively? I help organizations in two ways to do this. First, by immersive, human-centered and ethnographic storytelling through documentaries, videos and podcasts. Second, by training communities build sovereign citizen data and media.

<br>
<div style="text-align: center;">
  <div style="border: 2px solid black; padding: 10px; display: inline-block; font-size: 2em;">
    <div style="display: inline-block; border-left: 2px solid black; padding-left: 10px;">Select Projects</div>
  </div>
</div>
<br>

<section class="project-section">
  <style>
    .project-section {
      display: flex;
      flex-wrap: wrap;
      justify-content: space-between;
      padding: 0 15%;
    }

    .box {
      width: calc(33.33% - 10px); /* Fixed width for each box, 3 boxes per row with 10px spacing */
      margin-bottom: 10px;
      text-align: center;
      background-color: #FAE7EB; /* Default background color */
    }

    /* Random background colors for each box */
    .box:nth-child(1) { background-color: #FAE7EB; }
    .box:nth-child(2) { background-color: #E0D4E7; }
    .box:nth-child(3) { background-color: #BDD2E4; }
    .box:nth-child(4) { background-color: #EECEDA; }
    .box:nth-child(5) { background-color: #DBEEF7; }
    .box:nth-child(6) { background-color: #CCDCEB; }
    .box:nth-child(7) { background-color: #FEC89A; }
    .box:nth-child(8) { background-color: #FFDAB9; }
    .box:nth-child(9) { background-color: #E6E6FA; }
    .box:nth-child(10) { background-color: #FFB6C1; }

    .title {
      font-size: 1.5em;
      margin-top: 10px;
      font-weight: normal; /* Regular weight for the title */
    }

    .content-image {
      width: 100%; /* Image covers the entire width of the box */
      height: auto; /* Maintain aspect ratio */
      opacity: 0.7;
      transition: opacity 0.5s ease;
    }

    .content-image:hover {
      opacity: 1;
    }

    .content-text {
      text-align: left;
      padding: 5px; /* 5px padding around the text */
      width: 100%;
    }

    @media (max-width: 1024px) {
      .project-section {
        padding: 0 10%;
      }

      .box {
        width: calc(48% - 10px); /* 2 boxes per row on smaller screens */
      }
    }

    @media (max-width: 768px) {
      .project-section {
        padding: 0 5%;
      }

      .box {
        width: 100%; /* 1 box per row on mobile */
      }
    }
  </style>

  <div class="box">
    <a href="https://theofdn.org/openspeaks" target="_blank">
      <img src="/images/page-post/ps-img-openspeaks.gif" alt="OpenSpeaks logo" class="content-image">
    </a>
    <h3 class="title">OpenSpeaks</h3>
    <p class="content-text">Low-resourced language audiovisual documentation toolkit. Used in ten+ documentaries and to record ten+ languages.<br><em>Awards</em>: National Geographic Explorer (2017), MJ Bear Fellowship 2017, Creative Commons mini-grant, Mozilla Open Leadership (2021), Grant for the Web (2021)</p>
  </div>

  <div class="box">
    <img src="/images/page-post/ps-img-gyani-maiya.gif" alt="Gyani Maiya poster" class="content-image">
    <h3 class="title"><em>Gyani Maiya</em></h3>
    <p class="content-text"><u>Gyani Maiya</u><br>FILM / Directed and produced documentary contributing to revival of near-extinct Kusunda language.<br><em>Awards</em>: National Geographic Society grant; Nine international film festival wins and multiple worldwide screenings.</p>
  </div>

  <div class="box">
    <img src="/images/page-post/ps-img-marginalized-aadhaar.gif" alt="MarginalizedAadhaar poster" class="content-image">
    <h3 class="title"><em>MarginalizedAadhaar</em></h3>
    <p class="content-text">FILM / Founded and led research on the national biometric ID Aadhaar-led exclusion of India’s marginalized populations. I turned my findings into the eponymous feature-length documentary.
    <br>
    <em>Awards</em>: Digital Identity Fellowship (2019, by Yoti), premiere at re:publica 2021, Berlin</p>
  </div>

  <div class="box">
    <img src="/images/page-post/ps-img-bdam.gif" alt="Image 1" class="content-image">
    <h3 class="title"><em>Bringing Down a Mountain</em></h3>
    <p class="content-text">FILM / Directed and produced docufiction, narrating how India’s bahujan communities cease long-denied economic power from the dominant caste groups.<br><em>Awards</em>: Future I Money Artist grant (2023, Interledger Foundation)</p>
  </div>

  <div class="box">
    <a href="https://theofdn.org/openspeaks" target="_blank">
      <img src="/images/page-post/ps-img-edit-wikipedia.gif" alt="Learn to Edit Wikipedia" class="content-image">
    </a>
    <h3 class="title">Learn to Edit Wikipedia</h3>
    <p class="content-text">OER - WEB SERIES / Directed and produced two bilingual (<a href="https://www.youtube.com/watch?v=96Lzxglp5W4&list=PLe81zhzU9tTTuGZg41mXLXve6AMboaxzD">Hindi</a> and <a href="https://www.youtube.com/watch?v=8JRioZUEziQ&list=PLe81zhzU9tTSbi050RLGhRMXSoh-Meu77">Kannada</a>) educational web series on editing Wikipedia for the Centre for Internet and Society in collaboration with Christ University.</p>
  </div>

  <div class="box">
    <img src="/images/page-post/ps-img-ol-chiki.gif" alt="Project Ol Chiki poster" class="content-image">
    <h3 class="title">Project Ol Chiki</h3>
    <p class="content-text">Founded and coordinated input tools and Guru Gomke font family creation for India's Indigenous Santali language.</p>
  </div>
</section>

<p>
<hr>
</p>

# Reach out
Provide some details below to discuss a tailor-made plan for you.

<style>
  .contact-form-container {
    padding: 0 10%;
  }

  @media (min-width: 768px) {
    .contact-form-container {
      padding: 0 10%;
    }
  }
</style>

<div class="contact-form-container">
  {% include contact-form.html %}
</div>
<hr>

* _Image credits: Lead image by Mike Peel / CC BY-SA-4.0_.